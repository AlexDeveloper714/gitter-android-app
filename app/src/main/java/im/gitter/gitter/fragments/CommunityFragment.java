package im.gitter.gitter.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import im.gitter.gitter.CommunityRoomListAdapter;
import im.gitter.gitter.R;
import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.content.ContentProvider;
import im.gitter.gitter.content.RestResponseReceiver;
import im.gitter.gitter.content.RestServiceHelper;
import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.utils.CursorUtils;

public class CommunityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private static final int GROUP_LOADER = 0;
    private static final int ROOMS_LOADER = 1;

    private RestResponseReceiver restResponseReceiver = new RestResponseReceiver();
    private Cursor cursor;
    private String groupId;
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView recyclerView;
    private CommunityRoomListAdapter roomListAdapter;

    public static CommunityFragment newInstance(String groupId) {
        CommunityFragment fragment = new CommunityFragment();

        Bundle args = new Bundle();
        args.putString("groupId", groupId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        groupId = args.containsKey("groupId") ? args.getString("groupId") : savedInstanceState.getString("groupId");

        if (groupId == null) {
            throw new IllegalArgumentException("groupId required");
        }

        roomListAdapter = new CommunityRoomListAdapter(getActivity());

        getActivity().registerReceiver(restResponseReceiver, restResponseReceiver.getFilter());

        RestServiceHelper.getInstance().getRoomsForGroup(getActivity(), groupId);

        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community, container, false);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.caribbean, R.color.jaffa, R.color.ruby);

        recyclerView = (RecyclerView) view.findViewById(R.id.list_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(roomListAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        getActivity().setTitle(null);

        getLoaderManager().initLoader(GROUP_LOADER, null, this);
        getLoaderManager().initLoader(ROOMS_LOADER, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();

        RestServiceHelper.getInstance().getRoomList(getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                ((MainActivity) getActivity()).openDrawer();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unregisterReceiver(restResponseReceiver);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == GROUP_LOADER) {
            return new CursorLoader(getActivity(), Uri.withAppendedPath(ContentProvider.GROUPS_CONTENT_URI, groupId), null, null, null, null);
        }
        if (id == ROOMS_LOADER) {
            String sortOrder = Room.ROOM_MEMBER + " DESC, " +
                            Room.MENTIONS + " DESC, " +
                            Room.UNREAD_ITEMS + " DESC, " +
                            Room.LAST_ACCESS_TIME + " DESC";
            return new CursorLoader(
                    getActivity(),
                    ContentProvider.ROOMS_CONTENT_URI,
                    null,
                    Room.GROUP_ID + " = ?",
                    new String[] { groupId },
                    sortOrder);
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (loader.getId() == GROUP_LOADER) {
            if (cursor.moveToFirst() && this.cursor != cursor) {
                this.cursor = cursor;
                Group group = Group.newInstance(CursorUtils.getContentValues(cursor));

                getActivity().setTitle(group.getName());
            }
        } else {
            roomListAdapter.setCursor(cursor);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == GROUP_LOADER) {
            cursor = null;
        } else {
            roomListAdapter.setCursor(null);
        }
    }

    @Override
    public void onRefresh() {
        long requestId = RestServiceHelper.getInstance().getRoomsForGroup(getActivity(), groupId);
        restResponseReceiver.listen(requestId, new RestResponseReceiver.Listener() {
            @Override
            public void onResponse(int statusCode) {
                swipeLayout.setRefreshing(false);
            }
        });
    }
}
