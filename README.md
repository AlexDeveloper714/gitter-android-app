# gitter-android-app

Its Gitter, but on Android!

## Install prerequisites

 1. Clone and setup the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project
    - At a very minimum, `npm install`. You don't necessarily need to run the webapp locally and can skip the Docker stuff
 1. In the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project, run `npm run build-android-assets`
 1. Symlink the webapp embedded build asset output to the Android project
    - macOS: `mkdir -p mkdir app/src/main/assets && ln -s /Users/<YOUR_USERNAME>/Documents/gitlab/gitter-webapp/output/android/www /Users/<YOUR_USERNAME>/Documents/gitlab/gitter-android-app/app/src/main/assets/www`
    - Windows: `(mkdir app\src\main\assets || true) && mklink /D "C:\Users\<YOUR_USERNAME>\Documents\GitLab\gitter-android-app\app\src\main\assets\www" "C:\Users\<YOUR_USERNAME>\Documents\GitLab\webapp\output\android\www"`
 1. Make a copy of `secrets.properties.sample` named `secrets.properties` and follow the comment instructions inside
 1. Download the Android IDE: [Android Studio](http://developer.android.com/sdk/installing/studio.html) (you may need to install java by following the prompts)
    1. Optionally: Install the Android SDK: `brew install android-sdk`, Select the SDK that `brew` logged out back in the previous command
 1. Open this project with in Android Studio IDE
 1. The IDE will complain about "Gradle sync failed". Just do what it says.
 1. Once the IDE stops giving good suggestions go to **Tools** -> **Android** -> **SDK Manager** and do what the SDK Manager says.
 1. Once the SDK Manager stops giving good suggestions, use it to install the Google Repository and the Android Support Repository
 1. Your IDE should stop whining now.


## Using the emulator

### Setup

If you are using an Intel CPU, install the accelerator or emulation will be dog slow. If you are using Hyper-V on Windows, see the section below instead because Intel HAXM will refuse to install.

 1. Open the SDK Manager and download the Intel Emulator Accelerator, https://i.imgur.com/9viMhHQ.png https://i.imgur.com/Ds94V4a.png
 1. Go to your SDK_HOME and find the the correct executable that you just downloaded
    - macOS: `/usr/local/var/lib/android-sdk/extras/intel/Hardware_Accelerated_Execution_Manager/IntelHAXM_1.1.1_for_10_9_and_above.dmg`)
    - Windows: `C:\Users\<YOUR_USERNAME>\AppData\Local\Android\Sdk\extras\intel\Hardware_Accelerated_Execution_Manager\`
 1. Open the executable and install your new kernel extension

#### Emulator with Hyper-V installed on Windows 10

If you are using Hyper-V on Windows, follow the following steps derived from this post, https://blogs.msdn.microsoft.com/visualstudio/2018/05/08/hyper-v-android-emulator-support/

 1. Enable features **Hyper-V** and **Windows Hypervisor Platform**(you probably don't have this enabled already) in **Turn Windows features on or off** (requires a computer restart), https://i.imgur.com/99fnVrN.png
 1. Install Java Development Kit (JDK), http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
 1. Install Visual Studio Tools for Xamarin preview (15.8 Preview 1 or higher with Mobile development with .NET (Xamarin) workload), http://aka.ms/hyperv-emulator-dl
 1. In Visual Studio: [Tools -> Options -> Xamarin -> Android Settings](https://docs.microsoft.com/en-us/xamarin/android/troubleshooting/questions/android-sdk-location?tabs=vswin)
    - Android SDK location: `C:\Users\<YOUR_USRNAME>\AppData\Local\Android\Sdk`
 1. Create `C:\Users\<YOUR_USERNAME>\.android\advancedFeatures.ini` with the contents: `WindowsHypervisorPlatform = on`


### Running on a virtual device

 2. Open the AVD Manager (**Tools** -> **Android** -> **AVD Manager**), https://i.imgur.com/881HJHx.png
 3. Create a new Virtual Device. The size/model doesn't matter that much
    - Select a system image that has both a `x86_64` ABI and Google play services
 6. Finish and click play!



## Release

 1. Update `secrets.properties` from https://gitlab.com/gl-gitter/secrets/blob/develop/android/prod
 1. Update version. Remember that there are [two version identifiers](http://developer.android.com/tools/publishing/versioning.html), both of which are required.
    - `android:versionCode`: Integer that should increment with every release
    - `android:versionName`: Semver string like 1.2.3 that increments in the classic semver way
 1. In Android Studio, **Build** -> **Build APK(s)**, https://i.imgur.com/Eo0zGDj.png
 1. APK will be located at `app/build/outputs/apk/`
 1. TODO: Do we need to sign it for the Google Play store?


## FAQ

### `Emulator: emulator: ERROR: x86 emulation currently requires hardware acceleration!`

If you have Hyper-V enabled on Windows 10, the Intel HAXM accelerator will refuse to install. See the section above for using the emulator with Hyper-V installed on Windows 10

Make sure you have the **Windows Hypervisor Platform** feature enabled in **Turn Windows features on or off**


### Ignore quick boot saved state (cold boot, restoring)

See https://medium.com/@jughosta/quick-boot-for-android-emulator-8224f8c4ea01

 1. **Tools** -> **AVD Manager**
 1. Use the dropdown arrow for the given virtual device -> **Cold Boot Now**

![](https://i.imgur.com/Xu9AfD5.png)
